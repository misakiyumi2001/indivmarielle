<?php
include "dbconn.php";
if(isset($_POST["fullname"])&&isset($_POST["email"])&&isset($_POST["contactnumber"])&&isset($_POST["message"])){
  $sql="INSERT INTO contact (fullname,email,contactnumber,message)
  VALUES ('".$_POST["fullname"]."','".$_POST["email"]."','".$_POST["contactnumber"]."','".$_POST["message"]."')";
  mysqli_query($conn, $sql);
  mysqli_close($conn);
}
?>


<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" 
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/index.css">
    <title>Hello, world!</title>
    <style>
      .container {
      position: absolute;
      top: 30px;
      left:450px;
      color: white;
      width: 900px;
      padding: 20px;
      border-radius: 50px;
      font-weight: bold;
      }
      .LOGO {
        top: 50px;
        left: 280px;
        position: absolute;
        border-radius: 30px;
      }
      .background {
        height: 625px;
        width: 1366px;
      }
      #LOGO {
        height: 180px;
        width: 200px;
        border-radius: 30px;
      }
      .form-control {
        background: linear-gradient(to bottom, #ff66ff 0%, #ff0066 100%);
      }
      #box {
        background: linear-gradient(to top, #ff0066 0%, #000066 100%);
        border: none;
      }
      
}
    </style>
  </head>
  <body>
  <div class="LOGO">
	      	<img id="LOGO" src="../pictures/LOGO.png">
    </div>
	      	<img class="background" src="../pictures/background.jfif">
    <div class="container">
      <div class="row">
        <div class="col-4 offset-4">
        <h3 class="text-center">Contact Form</h3>
          <form action="" method="POST">
              <div class="form-group">
                <label for="fullname">Full Name:</label>
                <input type="text" name="fullname" id="" class="form-control" placeholder="Full Name">
              </div>

              <div class="form-group">
                <label for="fullname">Email:</label>
                <input type="text" name="email" id="" class="form-control" placeholder="Email">
              </div>
              <div class="form-group">
                <label for="fullname">Contact Number:</label>
                <input type="text" name="contactnumber" id="" class="form-control" placeholder="Contact Number">
              </div>

              <div class="form-group">
                <label for="fullname">Message:</label>
                <textarea type="text" name="message" id="" class="form-control" rows="5" placeholder="Message"></textarea>
              </div>

              <button type="submit" id="box" class="btn btn-primary">Submit</button>
          </form>
        </div>
      </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>
</html>